import javax.swing.*;
import java.awt.*;


public class Segment extends JPanel {
    private int xCord;
    private int yCord;
    private Segment precedent;

    public Segment(int x, int y, Color color, Segment precedent){
        xCord = x;
        yCord = y;
        this.setBackground(color);
        this.precedent = precedent;
    }

    public int getxCord(){
        return  xCord;
    }

    public int getyCord(){
        return yCord;
    }

    public Segment getPrecedent(){
        return precedent;
    }

    public void setxCord(final int xCord) {
        this.xCord = xCord;
    }

    public void setyCord(final int yCord) {
        this.yCord = yCord;
    }

    public void setPrecedent(final Segment precedent) {
        this.precedent = precedent;
    }
}
