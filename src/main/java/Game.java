import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;


public class Game extends JFrame implements Runnable {

    private Snake snake;
    private Maze maze;
    private Food food;
    private int dirX = 1, dirY = 0;

    public Game(int scale){
        setSize(new Dimension(800,800));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        maze = new Maze(new Dimension(800, 780), Color.BLACK, scale);
        food = new Food(new Random(80).nextInt()*scale, new Random(80).nextInt()*10, Color.RED);
        Segment head = new Segment(new Random(80).nextInt()*scale, new Random(80).nextInt()*scale, Color.BLUE, null);
        Segment segment = new Segment(head.getxCord()+scale/2, head.getyCord() + scale/2, Color.BLUE, null);
        ArrayList<Segment> segments = new ArrayList<>();
        segments.add(segment);
        snake = new Snake(head, segments);
        maze.paintScene(snake, food);
        add(maze);
    }

    @Override public void run() {

    }
}
