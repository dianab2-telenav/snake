import javax.swing.*;
import java.awt.*;


public class Maze extends JPanel {
    private int scale;
    public Maze(Dimension dimension, Color color, int scale){
        this.scale = scale;
        setLayout(null);
        this.setPreferredSize(dimension);
        this.setBackground(color);
    }

    public void paintScene(Snake snake, Food food){

        removeAll();
        food.setBounds(food.getxCord(), food.getyCord(), scale, scale);
        this.add(food);
        snake.getHead().setBounds(snake.getHead().getxCord(), snake.getHead().getyCord(), scale, scale);
        this.add(snake.getHead());
        snake.getBodySegments()
                .stream()
                .forEach(s -> { s.setBounds(s.getxCord(), s.getyCord(), scale, scale);
                this.add(s);});
        repaint();
        revalidate();
        validate();
    }
}
