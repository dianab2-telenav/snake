import javax.swing.*;
import java.awt.*;


public class Food extends JPanel {
    private int xCord;
    private int yCord;

    public Food(int x, int y, Color color){
        xCord = x;
        yCord = y;
        this.setBackground(color);
    }

    public int getxCord(){
        return  xCord;
    }

    public int getyCord(){
        return yCord;
    }
}
