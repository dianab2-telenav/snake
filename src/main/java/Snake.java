import javax.swing.*;
import java.util.ArrayList;


public class Snake {

    private Segment head;
    private ArrayList<Segment> bodySegments;
    private Thread thread = null;

    public Snake(Segment head, ArrayList<Segment> bodySegments) {
        this.head = head;
        this.bodySegments = bodySegments;
        this.bodySegments.get(0).setPrecedent(this.head);
    }

    public ArrayList<Segment> getBodySegments() {
        return bodySegments;
    }

    public Segment getHead() {
        return head;
    }

    public void addBodySegment(final Segment segment) {
        this.bodySegments.add(segment);
        this.bodySegments.get(bodySegments.size() - 1).setPrecedent(this.bodySegments.get(bodySegments.size() - 2));
    }



    public void moveBody() {
        bodySegments.stream().forEach(s -> {
            s.setxCord(s.getPrecedent().getxCord());
            s.setyCord(s.getPrecedent().getyCord());
        });
    }

    public void moveHead(int x, int y) {
        head.setxCord(head.getxCord() + x);
        head.setyCord(head.getyCord() + y);
    }


}
